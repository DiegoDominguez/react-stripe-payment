import 'bootswatch/dist/lux/bootstrap.min.css';
import './App.css';
import { loadStripe } from '@stripe/stripe-js';
import { Elements, CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import axios from 'axios';
import { useState } from 'react';

const stripePromise = loadStripe('pk_test_51KRJYUIFjgYqgZCpk1l7gcMM5pLYM7Ve1EQIsBJcs2c0y1PjMHnaZMHsKwuz9jBl8278bHWcQsvEegfw9WaZL5Xy00KYrYZUzF');

const CheckoutForm = () => {

  const stripe = useStripe();
  const elements = useElements();
  const [loading, setLoading] = useState(false);

  const handleSubmit = async(e) => {
    e.preventDefault();

    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: 'card',
      card: elements.getElement(CardElement)
    });

    setLoading(true);

    if (!error) {
      const { id: stripe_id } = paymentMethod;
      const monto = 20000;

      try {
        const { data } = await axios.post('http://127.0.0.1:8000/callejeritos/donaciones:crear_stripe_payment', {stripe_id, monto});
        console.log(data);
        elements.getElement(CardElement).clear();
        setLoading(false);
      } catch (error) {
        console.log(error.response.data.message);
        setLoading(false);
      }
    }
  };

  return (
    <form onSubmit={handleSubmit} className="card card-body">
      <img src="https://static.wixstatic.com/media/c3f4a4_2fc03f1db0204ecdbb756e094f682e2c~mv2.png/v1/fill/w_540,h_540,al_c/c3f4a4_2fc03f1db0204ecdbb756e094f682e2c~mv2.png" 
        alt="Redmi Airdots" className='img-fluid' />
      <div className="form-group">
        <CardElement className="form-control"/>
      </div>
      { !loading ? <button type='submit' className='btn btn-sm btn-primary mt-2' disabled={ !stripe }>Comprar</button> :
        <div className="spinner-border text-primary align-item-center" role="status">
          {/* <span className="sr-only">Loading...</span> */}
        </div> }
    </form>
  );
};


function App() {
    return ( 
      <Elements stripe={ stripePromise }>
        <div className="container p-4">
          <div className="row">
            <div className="col-md-4 offset-md-4">
            <CheckoutForm/>
            </div>
          </div>
        </div>
      </Elements>
    );
}

export default App;